# PhD Track informatique 2024

[TOC]

## Procédure d'admission du PhD Track

La procédure d'admission définie par IPParis est [présentée dans ces
slides](guide_PhD_Track_2023-2024.pdf). Vous trouverez ci-dessous une
adaptation de la procédure pour le PhD Track Informatique.


### Phase 1: Admission
Un Jury par parcours PhD Track étudie les dossiers de candidatures et choisit les candidats admissibles.


En informatique, ce jury est constitué du responsable du master, et des différents jurys de recrutement des parcours du master d'informatique, dont les responsables sont:
- [François Trahay](mailto:francois.trahay@telecom-sudparis.eu)
- [Natalia Kushik](mailto:natalia.kushik@telecom-sudparis.eu) (CSN)
- [Stephane Maag](mailto:stephane.maag@telecom-sudparis.eu) (CSN)
- [Sergio Mover](mailto:sergio.mover@polytechnique.edu) (CPS)
- [Olivier Levillain](mailto:olivier.levillain@telecom-sudparis.eu) (Cyber)
- [Louis Jachiet](mailto:louis.jachiet@telecom-paris.fr) (DataAI)
- [Adriana Tapus](mailto:adriana.tapus@ensta-paris.fr) (DS4Health)
- [Samuel Mimram](mailto:samuel.mimram@lix.polytechnique.fr) (FCS)
- [Gilles Schaeffer](mailto:schaeffe@lix.polytechnique.fr) (FCS)
- [James Eagan](mailto:james.eagan@telecom-paris.fr) (IGD)
- [Eric Lecolinet](mailto:eric.lecolinet@telecom-paristech.fr) (IGD)
- [Sourour Elloumi](mailto:sourour.elloumi@ensta-paris.fr) (MPRO)
- [Pierre Sutra](mailto:pierre.sutra@telecom-sudparis.eu) (PDS)
- [Gaël Thomas](mailto:gael.thomas@inria.fr) (PDS)

**Qui est concerné ?**: les responsables des parcours du master d'informatique, et le responsable du master d'informatique

**Comment fait-on ?**
Les dossiers sont en ligne sur [la plateforme HotCRP](https://cs.ip-paris.fr/hotcrp/phd23/).

#### Tri des dossiers
Dans un premier temps, on peut trier les candidatures en leur
affectant un ou plusieurs tags à chaque dossier:

- `csn, cps, cyber, dataai, ds4h, fcs, mpro, pds, igd`: la candidature devrait être évaluer par le jury du parcours X
  - Nouveautés par rapport à l'année dernière:
    - le parcours `hpda` a disparu: il a été intégré dans le parcours `pds`
    - le parcours `ds4h` apparait. Il s'agit du nouveau parcours [Digital Skills for Health Transformation](https://www.ip-paris.fr/en/education/masters/computer-science-program/master-year-2-digital-skills-health-transformation-ds4health) géré par Adriana

  - un dossier peut être évalué par plusieurs parcours. Par exemple, un étudiant qui voudrait faire de l'IA appliqué à la cybersécurité pourrait être taggé `[cyber]` et `[dataai]̀

- `math`: pour les étudiants du Phd track DataAI mais qui ont vocation à être intégrés dans le master de Maths

Vous pouvez dès maintenant ajouter/supprimer des tags aux différents dossiers afin de les attribuer aux différents parcours.


#### Evaluation des dossiers

Pour les dossiers attribués à vos parcours respectifs, vous pouvez commencer à écrire des reviews. La date limite pour évaluer tous les dossiers est le 2 février.

Vous pouvez trouver la liste de vos dossiers en cherchant votre tag,
par exemple `#pds` pour le parcours pds.  Pour chaque dossier, il
faudrait que designer plusieurs reviewers (au moins 2,
idéalement 3). Pour cela, cliquer sur `"assign reviews"`, sélectionnez
les reviewers et choisissez le Round "`Review`", puis cliquez sur "̀`Save assignments`".

S'il manque des reviewers, vous pouvez les inviter à partir de la partie [Users](https://cs.ip-paris.fr/hotcrp/phd23/users/all).

Les reviewers auront ensuite à évaluer des candidatures. Parmi les
informations à donner dans la review, il y a un nom de tuteur (d'un
labo d'IPParis) pour l'étudiant. Cela peut être un tuteur mentionné
par l'étudiant dans sa lettre, ou un tuteur travaillant sur la
thématique du projet de recherche du candidat.

* * *

### Phase 2: attribution des tuteurs.tutrices

Le Jury transmet les candidatures admissibles aux départements qui,
sur la base du profil de l’étudiant(e) et de ses motivations en lien
avec un projet de recherche et en concertation avec les laboratoires,
attribuent un choix de tutrices/tuteurs. Ces derniers doivent avoir
une bonne connaissance des cursus pédagogique proposés afin de bien
accompagner l’étudiant dans son cursus académique.

En informatique, le bureau IDIA se réunit en formation étendue avec
les référents phd tracks et les responsables de phd track et se charge
de l'attribution des tuteurs/tutrices. Le jury d'admission peut
suggérer un/des tuteurs en fonction du dossier de candidature (par
exemple lorsqu'un candidat souhaite travailler avec une personne en
particulier).

**Qui est concerné ?**: Le bureau IDIA, les référents PhD Track des
labos, les responsables de phd track.

* * *

### Phase 3: entretien avec le candidat(e)

Les référents PhD Track des labos prennent contact avec les
tuteurs/tutrices, leur transmettent les dossiers des candidats, et
leur expliquent le processus d'interview.

Les tutrices/tuteurs identifiés contactent l’étudiant(e) admissible
(entretien) afin de pouvoir l’évaluer et l’accompagner dans la
définition de son projet de recherche et son parcours pédagogique.

Après les échanges avec le/la candidat(e), la tutrice/le tuteur donne
son avis en remplissant une [Fiche projet](Fiche_projet.docx).

**Qui est concerné ?**: Les référents PhD Track des labos, les
tuteur/tutrices désignés lors de la phase 2.

#### Processus de l'entretien

L'entretien a deux objectifs:

- définir le projet de recherche du candidat et déterminer le tuteur
  le plus à même de l'encadrer

- compléter l'évaluation du candidat afin de pouovoir faire un
  interclassement des candidats lors de la phase 4

Pour cela, le/la/les tuteurs ont accès au dossier du candidat et aux
évaluations (cf phase 1). Le tuteur contacte le candidat par email
afin de le rencontrer en entretien. A la suite de l'entretien, la
tutrice/le tuteur remplit donne une [Fiche projet](Fiche_projet.docx) contenant:

- **Appréciation du candidat**: l'appréciation servira à
  interclasser les candidats qui sont probablement tous d'un bon
  niveau (car les dossiers faibles devraient avoir été éliminés lors
  de la phase 1). Il est donc demandé de juger le niveau du candidat
  de la manière suivante:

  - *Insuffisant*: le niveau du candidat est insuffisant pour le
    programme du PhD Track. Le tuteur recommande de ne pas accepter ce
    candidat en PhD Track.

  - *Bon*: le candidat a un bon niveau. Le tuteur recommande
    d'accepter ce candidat en PhD Track et s'engage à l'encadrer en PhD Track.

  - *Très bon*: le candidat est très bon. Parmi tous les candidats, ce
    candidat est dans le top 30%. Le tuteur recommande
    d'accepter ce candidat en PhD Track et s'engage à l'encadrer en PhD Track.

  - *Exceptionel*: le candidat est exceptionnel. Parmi tous les candidats, ce
    candidat est dans le top 10%. Le tuteur recommande
    d'accepter ce candidat en PhD Track et s'engage à l'encadrer en PhD Track.

- **Parcours**: si le candidat est accepté en PhD Track, il ou elle
  suivra des enseignements du master et sera intégré dans un des
  [parcours de la mention informatique](https://cs.ip-paris.fr/master/):
  - CSN (Computer Science for Networks)
  - CPS	(Cyber-Physical Systems)
  - Cyber (Cybersecurity)
  - DataAI (Data and Artificial Intelligence)
  - DS4Health (Digital Skills for Health Transformation)
  - FCS (Foundations of Computer Science/MPRI)
  - IGD (Interaction, Graphics & Design)
  - MPRO (Operations Research)
  - PDS (Parallel and Distributed Systems)


- Une **tutrice ou un tuteur** qui peut être différent de la personne
  qui a mené l'entretien. Dans ce cas, la tutrice ou le tuteur doit
  être consulté et s'engager à encadrer le candidat s'il est accepté
  en PhD Track (voir [Rôle de la tutrice/tuteur](#r%C3%B4le-de-la-tutricetuteur))
  
- **Bourse de vie/thèse**: le financement du PhD Track étant limité,
  tous les candidats acceptés ne pourront pas bénéficier d'une bourse
  de vie ou d'une bourse de thèse. Le tuteur peut s'engager à financer
  une bourse de vie ou de thèse en utilisant son propre budget (issu
  d'un projet de recherche type ANR, ou autre). Le candidat sera alors
  assuré d'être financé.
  


* * *

### Phase 4: interclassement

Les avis des tutrices/tuteurs sont discutés en Jury. Le Jury délibère
de façon concertée et collégiale, pour décider la liste des admis, et
établir un interclassement entre tous les candidats d’un parcours,
sans ex aequo, avant sa communication à la direction des formations
Master.

**Qui est concerné ?**: les responsables des parcours du master
d'informatique, le responsable du master d'informatique, les référents
PhD Track des labos, et le bureau IDIA.

* * *


### Phase 5: validation par le CcER

La direction des formations de Master, responsable du programme PhD
Track, s’occupe de l’évaluation et vérification de toutes les
admissions et la validation de la liste des admis par le CcER.


**Qui est concerné ?**: le [directeur des formations de Master](mailto:hani.hamzeh@ip-paris.fr), et le [responsable du master d'informatique](mailto:francois.trahay@telecom-sudparis.eu)

* * *


## Calendrier

Phase 1: admission
- 13/1: application deadline for the students
- 13/1 - 19/1: dispatch phase (the track chair assign students to the tracks and PC members to students)
- 19/1 at 2pm: first meeting of the PhD track jury (check that all the assignments are corrects)
	- **who?** heads of the tracks + head of the master
- 19/1 - 2/2: reviewing phase (each track reviews its students)
- 2/2 at 2pm: second meeting of the PhD track jury (decide which applications we accept, and at which level)
	- **who?** heads of the tracks + head of the master

Phase 2: attribution des tuteurs.tutrices
- 2/2 - 7/2: analysis by the IDIA bureau + référents laboratoires
- 7/2 at 2pm: first meeting of the IDIA bureau (check that we agree with the tutors/students for the interviews)
	- **who?** bureau IDIA

Phase 3: entretiens
- 7/2 - 26/2: interview phase
	- **who?** tutors

Phase 4: interclassement
- 26/2 at 2pm: second meeting of the IDIA bureau (harmonization)
	- **who?** bureau IDIA + heads of the tracks

Phase 5: validation par le CcER
- 28/2: deadline for the graduate school

* * *

## Roles

## Tutrices/tuteurs pendant la procédure d'admission

- L’identité de la tutrice/du tuteur devra être connue, dès la phase
  d’admission (engagement des Jurys et responsables de PhD Track
  d’échanger avec la tutrice/le tuteur pressenti(e)). La tutrice/le
  tuteur aura alors connaissance de son rôle (acceptation ou non de
  celui-ci), de ses obligations envers l’étudiant(e) et s’engagera sur
  ce rôle pour les années de Master ;

-  La tutrice/le tuteur organise et mène l’entretien d’admissibilité
   en collaboration avec le Jury ;

- La tutrice/le tuteur prend contact avec l’étudiant(e) avant son
  arrivée sur le campus ou début septembre afin d’établir le contrat
  pédagogique en collaboration avec l’étudiant(e), le responsable du
  parcours master et le secrétariat pédagogique ;

- La tutrice/le tuteur aura des contacts réguliers au moins mensuels
  avec l’étudiant(e) concerné(e) afin d’assurer un suivi sérieux et
  éclairé ;

- La tutrice/le tuteur échangera avec le secrétariat pédagogique dont
  dépend l’étudiant(e) et avec la personne en charge du programme PhD
  Track à la Graduate School : choix des cours, résultats académiques,
  difficultés rencontrées par l’étudiant(e), etc.

## Rôle de la tutrice/tuteur

-  Une fois l’étudiant(e) admis(e), celui-ci/celle-ci effectuera son
   travail de recherche en étant rattaché(e) à un laboratoire de
   recherche IP Paris et sera placé(e) sous la responsabilité d’une
   tutrice/d’un tuteur.

-  Au-delà de sa participation à la sélection des candidats, le rôle
   de la tutrice/du tuteur est d’accompagner et de conseiller
   l’étudiant(e), durant les deux premières années, aussi bien dans le
   choix de son cursus pédagogique (contrat pédagogique) que dans ses
   périodes d’immersion dans les laboratoires.

-  La tutrice/le tuteur, avec le responsable du parcours Master dans
   lequel l’étudiant(e) est inscrit(e), validera le contrat
   pédagogique.

- La tutrice/le tuteur remplira la fiche d’évaluation (de fin d’année
  pour les M1 et de milieu d’année pour les M2) en collaboration avec
  le responsable du PhD Track dans lequel l’étudiant(e) est admis(e)
  et le responsable du parcours Master dans lequel l’étudiant(e) est
  inscrit(e).

- La tutrice/le tuteur doit avoir une très bonne connaissance des
  maquettes pédagogiques du programme Master dans lequel l'étudiant(e)
  admis(e) s'inscrit. Elle/il pourra ainsi l'accompagner académiquement
  et pédagogiquement dans ses choix de cours et de parcours.

- La tutrice/le tuteur peut changer en cours de scolarité en fonction
  de l’évolution du projet de recherche de l’étudiant(e). Le
  changement doit être validé par le responsable du PhD Track ainsi
  que par la direction des formations de master supervisant le
  programme PhD Track puis confirmé auprès du secrétariat pédagogique
  dont dépend l’étudiant(e) et auprès de la personne en charge du
  programme PhD Track à la Graduate School.

- La tutrice/le tuteur pourra ou non devenir la directrice/le
  directeur de thèse suivant l’évolution du projet.

## Responsables PhD Track

Le rôle de chaque responsable PhD Track est :

- de proposer une composition de Jury pour la phase d’admission du PhD
  Track dont il a la charge puis d’organiser et piloter celle-ci – en
  collaboration avec les membres du Jury, les responsables de
  parcours, etc.;

- de piloter le classement sans ex aequo de tous les candidats (M1 et
  M2) dont l'admission est proposée au programme PhD Track, accompagné
  d’une proposition du type de bourses pour les candidats les mieux
  placés, afin de le soumettre à la commission d'arbitrage des bourses
  ;

- de piloter et synchroniser les :

  1. bilans de fin d’année (fiche évaluation) avec chacun(e) des
     tutrices/tuteurs (en collaboration avec les secrétariats
     pédagogiques et les responsables de parcours) pour les
     étudiant(e)s au niveau M1 ;

  2. bilans de milieu d’année (fiche évaluation) avec chacun(e) des
     tutrices/tuteurs (en collaboration avec les secrétariats
     pédagogiques et les responsables de parcours) pour les
     étudiant(e)s au niveau M2 ;

- De participer au recueil de données et à l'établissement de
  statistiques de réussite des étudiants du programme PhD Track ;

- de valider, auprès de la personne responsable du Programme PhD Track
  à la Graduate School, la liste des tutrices/tuteurs pour chaque
  étudiant(e) admis(e) – en mai/juin de l’année précédant la prochaine
  rentrée académique et d’orienter les tutrices/tuteurs vers la
  direction des formations de Master en cas de difficulté ;

- d’apporter une aide à la gestion des cas particuliers : abandon de
  scolarité, césure, redoublement, difficultés
  académiques/médicales/autres, changements de parcours/niveau, etc.

- d’assurer, tout au long de leur scolarité, un suivi des étudiant(e)s
  PhD Track – en collaboration avec les équipes pédagogiques, les
  secrétariats pédagogiques et la personne en charge du programme PhD
  Track à la Graduate School.

## Référents PhD Track des labos

Faire le lien entre les candidats retenus et les EC qui vont faire
l'interview (le cas échéant ce sont eux qui font l'interview).

Ils interviennent après la phase de sélection et avant les interviews.


* * *


# Qui a quel role ?


## Responsable des formations de Master
- [Hani Hamzeh \<hani.hamzeh@ip-paris.fr\>](mailto:hani.hamzeh@ip-paris.fr)

## Responsable du PhD Track informatique
-  [François Trahay \<francois.trahay@telecom-sudparis.eu\>](mailto:francois.trahay@telecom-sudparis.eu)

## Référents PhD Track des labos

- [Florence Tupin\<florence.tupin@telecom-paris.fr\>](mailto:florence.tupin@telecom-paris.fr)
- [Sebastian Will\<sebastian.will@polytechnique.edu\>](mailto:sebastian.will@polytechnique.edu)
- [Natalia Kushik \<natalia.kushik@telecom-sudparis.eu\>](mailto:natalia.kushik@telecom-sudparis.eu)
- [Goran Frehse \<goran.frehse@ensta-paristech.fr\>](mailto:goran.frehse@ensta-paristech.fr)



## Jury

En informatique, le jury est constitué du responsable du master, et des différents jurys de recrutement des parcours du master d'informatique, dont les responsables sont:

- [François Trahay \<francois.trahay@telecom-sudparis.eu\>](mailto:francois.trahay@telecom-sudparis.eu)
- [Natalia Kushik \<natalia.kushik@telecom-sudparis.eu\>](mailto:natalia.kushik@telecom-sudparis.eu)
- [Stephane Maag \<stephane.maag@telecom-sudparis.eu\>](mailto:stephane.maag@telecom-sudparis.eu)
- [Sergio Mover \<sergio.mover@polytechnique.edu\>](mailto:sergio.mover@polytechnique.edu)
- [Olivier Levillain \<olivier.levillain@telecom-sudparis.eu\>](mailto:olivier.levillain@telecom-sudparis.eu)
- [Louis Jachiet \<louis.jachiet@telecom-paris.fr\>](mailto:louis.jachiet@telecom-paris.fr)
- [Adriana Tapus \<adriana.tapus@ensta-paris.fr\>](mailto:adriana.tapus@ensta-paris.fr)
- [Samuel Mimram \<samuel.mimram@lix.polytechnique.fr\>](mailto:samuel.mimram@lix.polytechnique.fr)
- [Gilles Schaeffer \<schaeffe@lix.polytechnique.fr\>](mailto:schaeffe@lix.polytechnique.fr)
- [James Eagan \<james.eagan@telecom-paris.fr\>](mailto:james.eagan@telecom-paris.fr)
- [Eric Lecolinet \<eric.lecolinet@telecom-paristech.fr\>](mailto:eric.lecolinet@telecom-paristech.fr)
- [Sourour Elloumi \<sourour.elloumi@ensta-paris.fr\>](mailto:sourour.elloumi@ensta-paris.fr)
- [Pierre Sutra \<pierre.sutra@telecom-sudparis.eu\>](mailto:pierre.sutra@telecom-sudparis.eu)
- [Gaël Thomas \<gael.thomas@inria.fr\>](mailto:gael.thomas@inria.fr)

## Bureau IDIA

- [Olivier Bournez \<bournez@lix.polytechnique.fr\>](mailto:bournez@lix.polytechnique.fr)
- [Gilles Schaeffer \<schaeffe@lix.polytechnique.fr\>](mailto:schaeffe@lix.polytechnique.fr)
- [Talel Abdessalem \<talel.abdessalem@telecom-paris.fr\>](mailto:talel.abdessalem@telecom-paris.fr)
- [Francois Desbouvries \<francois.desbouvries@telecom-sudparis.eu\>](mailto:francois.desbouvries@telecom-sudparis.eu)
- [Maryline Laurent \<maryline.laurent@telecom-sudparis.eu\>](mailto:maryline.laurent@telecom-sudparis.eu)
- [Djamal Zeghlache \<djamal.zeghlache@telecom-sudparis.eu\>](mailto:djamal.zeghlache@telecom-sudparis.eu)
- [Amel Bouzeghoub \<amel.bouzeghoub@telecom-sudparis.eu\>](mailto:amel.bouzeghoub@telecom-sudparis.eu)
- [Benjamin Doerr \<doerr@lix.polytechnique.fr\>](mailto:doerr@lix.polytechnique.fr)
- [Thomas Bonald \<thomas.bonald@telecom-paris.fr\>](mailto:thomas.bonald@telecom-paris.fr)
- [Eric Goubault \<goubault@lix.polytechnique.fr\>](mailto:goubault@lix.polytechnique.fr)
- [David Filliat \<david.filliat@ensta-paris.fr\>](mailto:david.filliat@ensta-paris.fr)
- [Goran Frehse \<goran.frehse@ensta-paris.fr\>](mailto:goran.frehse@ensta-paris.fr)
- [François Fages \<francois.fages@inria.fr\>](mailto:francois.fages@inria.fr)
- [Samuel Mimram \<samuel.mimram@lix.polytechnique.fr\>](mailto:samuel.mimram@lix.polytechnique.fr)
- [Titus Zaharia \<titus.zaharia@telecom-sudparis.eu\>](mailto:titus.zaharia@telecom-sudparis.eu)
- [Djamel Belaid \<Djamel.Belaid@telecom-sudparis.eu\>](mailto:Djamel.Belaid@telecom-sudparis.eu)
- [Florence Tupin \<florence.tupin@telecom-paris.fr\>](mailto:florence.tupin@telecom-paris.fr)
- [François Trahay \<francois.trahay@telecom-sudparis.eu\>](mailto:francois.trahay@telecom-sudparis.eu)
- [Romain Alléaume \<romain.alleaume@telecom-paris.fr\>](mailto:romain.alleaume@telecom-paris.fr)
- [Ada Diaconescu \<ada.diaconescu@telecom-paristech.fr\>](mailto:ada.diaconescu@telecom-paristech.fr)
